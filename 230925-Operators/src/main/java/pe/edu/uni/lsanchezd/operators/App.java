/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.edu.uni.lsanchezd.operators;

import java.io.File;

/**
 *
 * @author Luis Sanchez <luis.sanchez.d@uni.pe>
 */
public class App {

    public static void main(String[] args) {
        System.out.print("-");
        System.out.println("Operators!");
        System.out.print("+");
        
        //operador preunario y postunario, promueve la variable y1
        int y1 = 4;
        double x1 = 3 + 2 * --y1;
        System.out.println("");
        
        /*
        
        El operador de division es de division entera,
        si hago 9/3.0  hay un entero y un double, el double
        promueve al entero
        
        */
              
       double x5 = 39.21;
       float y5 = 2.1f;
       
       System.out.println(x5 + y5);
       
       // El operador aritmetico suma promueve la variable de menor tamaño
       
       short x6 = 10;
       short y6 = 3;
       System.out.println(x6 / y6);
       
       // Se promueve a int cuando se trabaja con short 
       
       short x7 = 17;
       float y7 = 3;
       double z7 = 38;
        System.out.println(x7 * y7 / z7);
        // Existen tres promociones, de short a int, int a float, float a double y
        // las operaciones se realizan de izq a derecha
       
        
       boolean x8 = false;
       // tipo primitivo
       Boolean x25 = false;
       //Es una clase
       
        System.out.println("x8: "+x8);
        x8 = !x8;
        System.out.println("x8: "+x8);
       
       double x9 = 1.21;
        System.out.println("x9: "+x9);
        x9 = -x9;
        System.out.println("x9: "+x9);
        x9 = -x9;
        System.out.println("x9: "+x9);
        x9 = -x9;
        
        
        int counter = 0;
        System.out.println("counter: "+ counter);
        System.out.println("counter: "+ ++counter);
        System.out.println("counter: "+ counter);
        System.out.println("counter: "+ counter--);
        System.out.println("counter: "+ counter);
        
        
        int x = 3;
        int y = ++x * 5 / x-- + --x; // y = 4 * 5 / 4 -1 + 4 -1
        System.out.println("x: " + x);
        System.out.println("x: " + y);
        
        // El f al final de un numero se utiliza cuando es float
        // int x = 1.8
        // short y = 192542256
        // int z = 9f
        
        // long t = 192069545575623;
        
        /*
        int a1 = 1.8; 
        El valor 1.8 no es grande para un entero, el problemas
        no es la cantidad si no el tipo del numero es float, para eso se usa 
        el operador CAS, lo que se castea es el valor, no la variable*/
        
        int b1 = (int)1.8;
        System.out.println("b1: "+ b1);
        
        short b2 = (short)192542256;
        System.out.println("b2: "+ b2);
        /* Overload: Mas info de la que puedo reibir
        Underload: Menos info 
        */
        
        int b3 = (int)9f;
        System.out.println("b3: "+b3);
        /* Cuando hay un exceso de bit no se refleja el
        mis mo valor que cuando no hay un exceso.
        Cuando no hay un exceso se refleja el mismo valor
        */
        
        long t = 192069545575623L;
        System.out.print("t: "+t);
        /* No necesariamente se necesita hacer un cast
            la parte del ln en un "println" sirve para hacer el salto de linea
        */
        
        
        
        
        short g2 = 10;
        short g3 = 3;
        short g4 = (short)(g2 * g3);
        System.out.print("g4: "+g4);
        
        
        /* El resultado ya ha sido promocionado
        por la operacion, que seria un int, entonces
        deberia definirse la operacion como un int, o podemos castearlo
        El valor no cambia ya que los valores no se han excedido de lo 
        que la variable es posible de soportar
        
        short g2 = 10;
        short g3 = 3;
        short g4 = g2 * g3;
        
        */
        
        
        // OPERACIONES DE COMPOSICION
        
        // En la misma linea se pueden hacer varias instanciaciones en la misma linea
        // separadas por comas
       
        int g5 = 2, f6 = 3;
        g5 = g5 * f6;   // asignacion simple (=) no es buena practica ya que se llama a la misma var 2 veces
        g5 *= f6;       // asignacion compuesta (*=, /=, %=, ...)es bunea ractica
        System.out.println("g3: "+ g5);
        
        
        // El cas se utiliza cuando no se necesita el valor exacto en caso el valor sea mas grande que el soportado
        
        long a4 = 18;
        int h4 = 5;
        
        /*
        b4 = b4 * a4;
        se promueve a long, pero b4 es int por eso sale error
        */
        
        h4 *= a4;
        
        // La asignacion compuesta lo autocastea 
        
        System.out.println("h4: "+h4);
        
        
        
        long a5 = 5;
        long b5 = (a5 = 3); // La asignacion se realiza a los dos, si se quita los partentesis daria lo mismo por el igual
        
        System.out.println("a5:"+a5);
        System.out.println("b5:"+b5);
        
        // ESTOS SON JUEGOS DE LA SINTAXIS DE JAVA
        
        
        
        
        
        // OPERADORES DE COMPARACION
        
        // Se compara numeros, pero su resultado es un booleano no un numero
        int a6 = 10, b6 = 20, c6 = 30;
        
        System.out.println(a6 < b6);
        System.out.println(a6 <= b6);
        System.out.println(a6 >= b6);
        System.out.println(a6 > b6);
        
        boolean a7 = true || (y < 4);
        System.out.println("a7:"+a7);
        /*
        
        cond 1 | cond 2 : Se evaluan ambos operandos de cada lado
        cond 1 || cond 2 : Si en un operando se tiene la respuesta, el otro lado ya no se evalua
        
        Es como para ahorra un paso si se puede
        
        */
        
        Object o; // Es el objeto raiz de java, se declara la variable
        
        /* Todos los objetos de java viene a partir de esta clase
       
        */
        
        o = new Object(); // Este constructor no tiene ningun parametro, en este codigo se instancia la clase
      
        if (o != null && o.hashCode() < 5 ){
            // do something
        }
        if (o != null & o.hashCode() < 5){
            // do something
        }
        
        // Sucede lo mismo que con el operador OR
        
        
        int a8 = 6;
        boolean b8 = (a8 >= 6) || (++a8 <= 7);
        System.out.println("a8: "+a8);
        
        
        boolean b9 = false;
        boolean a9 = (b9 = true);
        System.out.println("a9: "+a9);
        
        
        
        // OPERADOR DE IGUALDAD
        
        System.out.println("Equality operator");
        File p1 = new File("file.txt"); //Se instancia
        File q1 = new File("file.txt"); //Se instancia
        File r1 = p1;
        
        // Esto es referencia
        
        System.out.println("p1 == q1? " + (p1 == q1));
        System.out.println("p1 == r1? " + (p1 == r1));
        
        /* La primera es false ya que puede apuntar al mismo
        archivo pero no ocupa el mismo espacio en la memoria
        La segunda es el mismo espacio en la memoria
        */
        
        
        
        System.out.println("Conditional statements!");

        int j = 28;
        if (30 < j) {
            System.out.println("el numero es mayor a 30");
        } else {

            if (j % 2 == 1) {
                System.out.println("Es impar");

            } else {

                if (j % 2 == 0) {
                    System.out.println("Es par");
                }

            }
        }

        System.out.println("+++++++++++++++++++++++++++++++++++++++");
        int k = 31;
        if (30 < k) {
            System.out.println("el numero es mayor a 30");
            return;
        }

        if (k % 2 == 1) {
            System.out.println("Es impar");
            return;

        }

        if (k % 2 == 0) {
            System.out.println("Es par");
            return;
        }

        // este codigo es mas legible, el codigo anterior es menos legible ya que tiene mas llaves
        int i = 2;
        int m = 11;
        switch(i%2) {
            
            case 0:
                System.out.println("Es par");
                break;
            case 1:
                System.out.println("Es impar");
                break;
            default:
                System.out.println("Cualquier otro numero");
                
     
        }
        
        
        
        
        
        
        
        
    }
}
